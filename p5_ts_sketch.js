const p5_ts_sketch = (p55) => {
// Fourier Series
// Daniel Shiffman
// https://thecodingtrain.com/CodingChallenges/125-fourier-series.html
// https://youtu.be/Mm2eYfj0SgA
// https://editor.p5js.org/codingtrain/sketches/SJ02W1OgV
// minor mods to work with pyviz: https://discourse.holoviz.org/t/p5-js-example/1551/5

let time    = 0;
let wave    = [];
let x_vals  = [];
let looping = true;

p55.setup = () => {
    let canvas = p55.createCanvas(600, 400);
    looping = true;
    console.log("Created Canvas" + canvas)
};
p55.draw = () => {
  p55.background(0);
  p55.translate(150, 200);
  mouse_x.value = p55.mouseX;
  mouse_y.value = p55.mouseY;

  let x = 0;
  let y = 0;
  let n = number_of_terms.value;

  // draw the vectors and circles
  for (let i = 0; i < n; i++) {
    let prevx = x;
    let prevy = y;

    let n = i * 2 + 1;
    let radius = 75 * (4 / (n * Math.PI));
    x += radius * Math.cos(n * time);
    y += radius * Math.sin(n * time);

    p55.stroke(255, 100);
    p55.noFill();
    p55.ellipse(prevx, prevy, radius * 2, radius * 2);

    p55.stroke(255);
    p55.line(prevx, prevy, x, y);
  }
  x_vals.unshift(x);
  wave.unshift(y);

  // draw the path being traced out
  p55.beginShape();
  p55.noFill();
  for (let i = 0; i < wave.length; i++) {
    p55.vertex(x_vals[i], wave[i] );
  }
  p55.endShape();

  // draw the generated function
  p55.translate(200, 0);
  p55.line(x - 200, y, 0, wave[0]);

  p55.beginShape();
  p55.noFill();
  for (let i = 0; i < wave.length; i++) {
    p55.vertex(i, wave[i] );
  }
  p55.endShape();

  if (wave.length > 250) {
    wave.pop();
    x_vals.pop()
  }

  time += 0.05;

};
// --------------------------------------------------------------------------------------------
p55.checkLoop = () => {
  if (this.checked()) {
    p55.loop();
  } else {
    p55.noLoop();
  }
};
// --------------------------------------------------------------------------------------------
p55.mousePressed = () => {
  p55.noLoop();
};
// --------------------------------------------------------------------------------------------
p55.mouseReleased = () => {
  p55.loop();
};
// --------------------------------------------------------------------------------------------
p55.mouseClicked = () => {
  mouse_clicks.value += 1;
  if (looping ) {
    p55.noLoop();
    looping = false;
  } else {
    p55.loop();
    looping = true;
  }
};
// --------------------------------------------------------------------------------------------
p55.keyPressed = () => {
    console.log( key );
    //mouse_x.value += 1;
};
// --------------------------------------------------------------------------------------------
};

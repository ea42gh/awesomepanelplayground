#!/usr/bin/python3

from twisted.internet.defer import inlineCallbacks
from twisted.logger import Logger

from autobahn.twisted.util import sleep
from autobahn.twisted.wamp import ApplicationSession
from autobahn.wamp.exception import ApplicationError

import numpy as np

def measurement(t, slope, c):
    return c + slope*t + 0.5*np.random.normal()

class Sensor_1(ApplicationSession):

    log = Logger()

    def __init__(self, config):
        ApplicationSession.__init__(self, config)
        self.reset()

    def reset(self):
        self.slope  = .4
        self.offset = .3;
        self.t      =  0.
        self.v      =  measurement( self.t, self.slope, self.offset )

    @inlineCallbacks
    def onJoin(self, details):

        ## SUBSCRIBE to a topic and receive events
        ##
        def onhello(msg):
            self.log.info("Sensor<1> onhello: {msg}", msg=msg)

        sub = yield self.subscribe(onhello, 'com.example.onhello')
        self.log.info("Sensor<1> subscribed to topic 'onhello'")

        ## REGISTER a procedure for remote calling

        def actuate_1_msg(x):
            '''No return value: this function can trigger on receipt of a message'''
            self.offset = self.v + self.slope*0.5 # x
            self.t      = 0.
            self.slope *= -1.
            self.log.info("******************** exec actuate_1 [_msg] ({x}) {v}", x=x, v=self.slope )

        def actuate_1_rpc(x):
            '''return value: this function can be called as a remote procedure'''
            self.log.info("******************** exec actuate_1_msg({x})", x=x )
            actuate_1_msg(x)
            return True

        reg = yield self.subscribe(actuate_1_msg, 'com.example.actuate_1_msg')
        self.log.info("Sensor<1> subscribed to actuate_1_msg")

        act = yield self.register(actuate_1_rpc, 'com.example.actuate_1_rpc')
        self.log.info("Sensor<1> registered 'actuate_1_rpc' to execute actuate_1_rpc")

        ## PUBLISH and CALL every 3 seconds .. forever
        counter = 0
        while True:

            ## PUBLISH an event
            v = measurement( self.t, self.slope, self.offset )
            self.t +=1.
            self.v = v

            yield self.publish('com.example.measurement_1', v)
            if True: #counter < 3:
                self.log.info("******************* Sensor<1> published 'measurement_1' with {v}",
                          v=v)
            counter += 1

            ## call remote procedure  mul2
            #try:
            #    res = yield self.call('com.example.mul2', counter, 3)
            #    self.log.info("mul2() called with result: {result}",
            #                  result=res)
            #except ApplicationError as e:
            #    ## ignore errors due to the frontend not yet having
            #    ## registered the procedure we would like to call
            #    if e.error != 'wamp.error.no_such_procedure':
            #        raise e

            yield sleep(0.8)

if __name__ == '__main__':
    import os
    from autobahn.twisted.wamp import ApplicationRunner

    url   = os.environ.get('CBURL',   'ws://localhost:8060/ws')
    realm = os.environ.get('CBREALM', 'sensorrealm')

    runner = ApplicationRunner(url=url, realm=realm )
    runner.run( Sensor_1, auto_reconnect=True)

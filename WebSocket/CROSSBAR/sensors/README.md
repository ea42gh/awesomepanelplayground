Modifications to one of the crossbar.io demos

Separate processes:

* Sensor_1  generates random measurements forwarded to the router<br>
actuator_1(x) modifies the measurement generation
* Sensor_2  generates random measurements forwarded to the router<br>
* index.html monitors the message traffic if desired


```sh
# start the router and the sensor processes
crossbar start
```
To monitor the message traffic, optionally open a view of the console for
```
http://localhost:8060
```
Some information about the crossbar instance is at
```
http://localhost:8060/info
```

**Remarks**
* I do not yet understand config.json: interchanging the sensors causes only one of them to start

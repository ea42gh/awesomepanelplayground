#!/usr/bin/python3

import numpy as np

from autobahn.asyncio.component import Component, run
from asyncio import sleep
#from autobahn.wamp.types import RegisterOptions
#from autobahn.wamp.exception import ApplicationError

import os

url   = os.environ.get('CBURL',   u'ws://localhost:8060/ws')
realm = os.environ.get('CBREALM', u'sensorrealm')

comp = Component(
    transports = url,
    realm      = realm,
)

def noisy_sine( t ):
    return 0.3*np.random.normal() + np.sin( np.pi * 0.05*t)

@comp.on_join
async def joined( session, details):
    print( f"Sensor<2> joined {details}" )
    counter = 0

    # ---------------------------------- Subscription ----------------
    def _on_hello(msg):
        #self.log.info("Sensor<2> 'onhello' received: {msg}", msg=msg)
        print(f"Sensor<2> 'onhello' received: {msg}")

    session.subscribe(_on_hello, u"com.example.onhello")
    print("Sensor<2> subscribed to topic 'onhello'")

    while True: #session.is_connected():
        print(".", end='')
        v = noisy_sine(counter)
        session.publish(u'com.example.measurement_2', v)
        print(f"******************* Sensor<2> published 'measurement_2' with {v}" )

        counter += 1
        await sleep(0.1)

if __name__ == '__main__':

    run( [ comp ] )

#!/usr/bin/env python3

# WS server that sends messages at random intervals

import asyncio
import random
import numpy as np
import json
import websockets
# serve a dictionary {"data": varying_length array } on a given port
import sys

async def produce(websocket, path):

    while True:
        n = random.randint(1,10);
        l = { 'data': [(i,x) for i,x in enumerate(np.random.normal(size=n))] }
        #l =  { 'data': np.random.normal(size = n).tolist() }

        await websocket.send( json.dumps(l, ensure_ascii=False) )
        await asyncio.sleep(random.random() * 3)

# ========================================================================
if len(sys.argv) == 1:
    print( f"Usage: {sys.argv[0]} port_number   (argument required)" )
    exit(-1)
else:
    try:
        port = int(sys.argv[1])
    except:
        print( f"Usage: {sys.argv[0]} port_number  ((argument must be an integer" )
        print( ".   port number must be an integer")
        exit(-2)

# ========================================================================
start_server = websockets.serve( produce, "127.0.0.1", port)

asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()

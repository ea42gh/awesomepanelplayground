#!/usr/bin/env python3

# WS server that sends messages at random intervals

import asyncio
import random
import numpy as np
import json
import websockets

async def produce(websocket, path):
    l =  [1.1,2.2,'troglodyte', -3.3,4.4,5.,6.,7.,8.8]

    while True:
        n = random.randint(1,len(l));
        await websocket.send( json.dumps(l[0:n], ensure_ascii=False) )
        await asyncio.sleep(random.random() * 3)

start_server = websockets.serve( produce, "127.0.0.1", 9090)

asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()
